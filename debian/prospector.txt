NAME
  prospector - static Python code analyzer

SYNOPSIS
  $ prospector

  $ prospector <path/to/project>

  $ prospector <path/to/module.py> <path/to/another/module.py>

DESCRIPTION
 Prospector analyzes Python source files and puts out information about
 coding errors, potential problems, convention violation and unnecessary
 complexity. It provides an uniform and flexible interface to a number of
 tools for static Python code checking.

 By default, Prospector uses the following tools:
 Dodgy,
 McCabe (a Flake8 extension),
 Pydocstyle (successor of the tool pep257),
 Pycodestyle (successor of the tool pep8),
 Pep8-naming (a Flake8 extension),
 Pyflakes, and
 Pylint.
 It auto detects libraries a project uses, to which it could adapt
 (currently, Celery, Django and Flask are supported individually).
 Additionally, Vulture and Pyroma could be employed for testing.

 Prospector could be adjusted very detailed to personal coding styles
 through profiles. For comprehensive information on this application,
 please see: /usr/share/doc/prospector/html/index.html.

OPTIONS
  -s,--strictness <veryhigh,high,medium,low,verylow>

     switch reporting level (default: medium)

  -D,--doc-warnings

     include warnings about documentation (run with pep257)

  -o,--output-format <emacs,vscode,grouped,json,pylint,text,xunit,yaml>

     switch output format (default: grouped)

  -w,--with-tool <tools>

     a tool or a list of tools to run in addition to the default tools
     (use this to run with vulture or pyroma)

  -P,--profile <profile>

     one or more profiles to be used (configuration files in YAML format)
      
  --help

     show a full list of command line options and flags

SEE ALSO
  dodgy(1), pydocstyle(1), pep8(1), pyflakes(1), pylint(1), vulture(1), pyroma(1)

AUTHORS
  Prospector is a spinoff from Landscape.io written by
  Carl Crowder <emailme@carlcrowder.com>, Jeff Quast <contact@jeffquast.com>,
  Jason Simeone <jay@classless.net>, and Sam Spilsbury <smspillaz@gmail.com>
